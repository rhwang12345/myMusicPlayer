# myMusicPlayer

#### 介绍
练习一下QML
1.实现了搜索音乐、播放音乐、历史记录、我喜欢等功能
2.大量运用了RowLayout、ColumnLayout、Grid等布局
3. 使用了ListView进行列表展示
4.使用MediaPlayer实现媒体播放
5.使用Settings实现数据存储
6.实现了轮播图等动画
7.实现了c++和qml的交互功能
详细说明：https://www.yuque.com/rhwang12345/znwk2u/xcgx3r0u6bhwwv8f?singleDoc# 《用QML写一个音乐播放器》
